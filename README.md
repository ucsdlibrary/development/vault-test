# Vault Test

This small application was written to address 2 use cases.

- Confirm that a newly created Vault secret has the correct policy and role configuration in place
- Confirm that the Vault kubernetes agent is running as expected in a given cluster.

## Usage
This command line application takes a required set of arguments as shown in the example below:

```sh
vault-test -kubeconfig /home/mcritchlow/.kube/configs/rancher-ucsd-review -role highfivereview -namespace highfive-review -secret applications/highfive/review -sa highfive
```

Running the application with no arguments will provide usage instructions:

```sh
Usage: vault-test -kubeconfig /path/to/config -role vault-role -namespace k8s-namespace -secret vault/secret/path -sa k8s-serviceaccount
  -kubeconfig string
    	Absolute path to your kubeconfig file
  -namespace string
    	Kubernetes namespace
  -role string
    	Vault role name
  -sa string
    	Kubernetes serviceAccountName
  -secret string
    	Vault Secret path
```

## Local Development
You will need Go installed and configured for your editor of choice. In particular you'll benefit greatly from having the following installed:
- [gopls LSP client][gopls] for completion, suggestions, documentation, etc.
- [gofmt][gofmt] for code formatting/linting
- [goimports][goimports] for automatically adding and maintaining `import` statements

You will also need an [EditorConfig][editorconfig] plugin installed for your editor to ensure your Go files follow the project syntax.

To run locally:

```sh
go run ./main.go
```

[editorconfig]:https://editorconfig.org/
[gopls]:https://pkg.go.dev/golang.org/x/tools/gopls
[gofmt]:https://pkg.go.dev/cmd/gofmt
[goimports]:https://pkg.go.dev/golang.org/x/tools/cmd/goimports
