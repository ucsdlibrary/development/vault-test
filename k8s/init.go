package k8s

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

type config struct {
	clientSet      *kubernetes.Clientset
	role           string
	namespace      string
	vaultSecret    string
	serviceAccount string
}

// public entrypoint for test deployment
func TestVault(kubeconfig, role, namespace, vaultSecret, serviceAccount string) {
	c := &config{clientSet(kubeconfig), role, namespace, vaultSecret, serviceAccount}
	log.Println("Starting vault-test using:")
	log.Printf("namespace: %s\n", c.namespace)
	log.Printf("role: %s\n", c.role)
	log.Printf("vaultSecret: %s\n", c.vaultSecret)
	log.Printf("serviceAccountName: %s\n", c.serviceAccount)

	//setup parent context for program
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() //cleanup when TestVault exits

	//check if service account exists, if not create it
	serviceAccountExists := c.checkServiceAccount(ctx)

	job, err := c.clientSet.BatchV1().Jobs(c.namespace).Create(ctx, c.getJobObject(), metav1.CreateOptions{})
	if err != nil {
		log.Fatalln(err.Error())
	}
	c.watchJob(ctx)

	c.deleteJob(ctx, job.Name)

	//if we had to create the serviceAccount, clean up after ourselves and delete it
	if !serviceAccountExists {
		c.deleteServiceAccount(ctx)
	}
}

func (c config) deleteServiceAccount(ctx context.Context) {
	err := c.clientSet.CoreV1().ServiceAccounts(c.namespace).Delete(ctx, c.serviceAccount, metav1.DeleteOptions{})
	if err != nil {
		log.Fatalln(err.Error())
	}
}

// If the specified serviceAccount doesn't exist, we want to create it
func (c config) checkServiceAccount(ctx context.Context) bool {
	serviceAccountExists := true
	_, err := c.clientSet.CoreV1().ServiceAccounts(c.namespace).Get(ctx, c.serviceAccount, metav1.GetOptions{})
	if err != nil {
		log.Printf("Service Account %s does NOT exist in namespace %s\n", c.serviceAccount, c.namespace)
		serviceAccountExists = false
		_, err := c.clientSet.CoreV1().ServiceAccounts(c.namespace).Create(ctx, &v1.ServiceAccount{ObjectMeta: metav1.ObjectMeta{Name: c.serviceAccount, Namespace: c.namespace}}, metav1.CreateOptions{})
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

	return serviceAccountExists
}

func (c config) jobWatcher(ctx context.Context) watch.Interface {
	watcher, err := c.clientSet.
		BatchV1().
		Jobs(c.namespace).
		Watch(
			ctx,
			metav1.ListOptions{
				TypeMeta:      metav1.TypeMeta{},
				LabelSelector: "app=vault-test",
				FieldSelector: "",
			},
		)
	if err != nil {
		log.Fatalln(err.Error())
	}

	return watcher
}

// check for job status.
func (c config) watchJob(ctx context.Context) {
	watchCtx, cancel := context.WithTimeout(ctx, 20*time.Second)
	defer cancel()
	watcher := c.jobWatcher(ctx)

	for {
		select {
		case event := <-watcher.ResultChan():
			job := event.Object.(*batchv1.Job)

			if job.Status.Succeeded > 0 {
				log.Printf("Job: %s succeeded!\n", job.Name)
				c.printLogs(watchCtx)
				watcher.Stop()
				return
			}
		case <-watchCtx.Done():
			watcher.Stop()
			c.deleteJob(ctx, "vault-test") //job failed to succeed, delete it
			log.Fatalln("Exit from watchDeploy for Job vault-test because the context is done")
		}
	}
}

func (c config) deleteJob(ctx context.Context, jobName string) {
	log.Println("Deleting deployment...")
	deletePolicy := metav1.DeletePropagationForeground
	err := c.clientSet.BatchV1().Jobs(c.namespace).Delete(ctx, jobName, metav1.DeleteOptions{PropagationPolicy: &deletePolicy})
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func (c config) printLogs(ctx context.Context) {
	pods, err := c.clientSet.CoreV1().Pods(c.namespace).List(ctx, metav1.ListOptions{LabelSelector: "job-name=vault-test"})
	if err != nil {
		log.Fatalln(err.Error())
	}
	for _, v := range pods.Items {
		request := c.clientSet.CoreV1().Pods(c.namespace).GetLogs(v.Name, &v1.PodLogOptions{})
		podLogs, err := request.Stream(ctx)
		if err != nil {
			log.Fatalln(err.Error())
		}
		defer podLogs.Close()
		buf := new(bytes.Buffer)
		_, err = io.Copy(buf, podLogs)
		if err != nil {
			log.Fatalln(err.Error())
		}
		log.Println("/vault/secrets/test.env secret file contents: ")
		log.Println(buf.String())
	}
}

func clientSet(kubeconfig string) *kubernetes.Clientset {
	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		log.Fatalln(err.Error())
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalln(err.Error())
	}

	return clientset
}

func (c config) getJobObject() *batchv1.Job {
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "vault-test",
			Namespace: c.namespace,
			Labels: map[string]string{
				"app": "vault-test",
			},
		},
		Spec: batchv1.JobSpec{
			Template: v1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: c.getAnnotations(),
				},
				Spec: v1.PodSpec{
					Containers: []v1.Container{
						{
							Name:    "vault-test",
							Image:   "busybox:latest",
							Command: []string{"cat", "/vault/secrets/test.env"},
						},
					},
					RestartPolicy:      v1.RestartPolicyNever,
					ServiceAccountName: c.serviceAccount,
				},
			},
		},
	}
}

// func getAnnotations(role, vaultSecret string) map[string]string {
func (c config) getAnnotations() map[string]string {
	return map[string]string{
		"vault.hashicorp.com/agent-pre-populate-only":        "true",
		"vault.hashicorp.com/agent-inject":                   "true",
		"vault.hashicorp.com/role":                           c.role,
		"vault.hashicorp.com/agent-inject-secret-test.env":   c.vaultSecret,
		"vault.hashicorp.com/agent-inject-template-test.env": c.getSecretTemplate(),
	}
}

func (c config) getSecretTemplate() string {
	var template strings.Builder
	fmt.Fprintf(&template, "{{- with secret \"%s\" -}}\n", c.vaultSecret)
	template.WriteString("{{- range $k, $v := .Data.data }}\n")
	template.WriteString("export {{ $k | toUpper }}='{{ $v }}'\n")
	template.WriteString("{{- end }}\n")
	template.WriteString("{{- end }}")
	return template.String()
}
