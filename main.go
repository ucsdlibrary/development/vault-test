package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/ucsdlibrary/development/vault-test/k8s"
)

func main() {
	var kubeconfig, role, namespace, vaultSecret, serviceAccount string

	flag.StringVar(&kubeconfig, "kubeconfig", "", "Absolute path to your kubeconfig file")
	flag.StringVar(&role, "role", "", "Vault role name")
	flag.StringVar(&namespace, "namespace", "", "Kubernetes namespace")
	flag.StringVar(&vaultSecret, "secret", "", "Vault Secret path")
	flag.StringVar(&serviceAccount, "sa", "", "Kubernetes serviceAccountName")

	flag.Parse()

	if len(kubeconfig) == 0 || len(role) == 0 || len(namespace) == 0 || len(vaultSecret) == 0 || len(serviceAccount) == 0 {
		fmt.Println("Usage: vault-test -kubeconfig /path/to/config -role vault-role -namespace k8s-namespace -secret vault/secret/path -sa k8s-serviceaccount")
		flag.PrintDefaults()
		os.Exit(1)
	}

	k8s.TestVault(kubeconfig, role, namespace, vaultSecret, serviceAccount)
}
