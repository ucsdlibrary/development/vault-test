# renovate: datasource=docker depName=alpine
ARG ALPINE_VERSION=3.16
# renovate: datasource=docker depName=golang
ARG GO_VERSION=1.19.4

FROM golang:$GO_VERSION-alpine$ALPINE_VERSION AS builder
ENV CGO_ENABLED 0
ENV GOOS linux
ENV GOARCH amd64
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/gitlab.com/ucsdlibrary/development/vault-test
COPY . .
# Fetch dependencies.
# Using go get.
RUN go get -d -v
# Build the binary.
RUN go build -ldflags="-w -s" -o /go/bin/vault-test

FROM scratch as production
# Copy our static executable.
COPY --from=builder /go/bin/vault-test /go/bin/vault-test
# Run the vault-test binary.
ENTRYPOINT ["/go/bin/vault-test"]

